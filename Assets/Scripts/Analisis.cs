﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Analytics;
using UnityEngine;

public static class Analisis
{
    /// <summary>
    /// Crea el evento para el inicio de una partida.
    /// </summary>
    /// <param name="dificultad"></param>
    public static void InicioPartida(int dificultad, int modo, int cubo)
    {
        Analytics.CustomEvent("inicioPartida", new Dictionary<string, object>
        {
            { "dificultad", dificultad},
            { "modoJuego", modo},
            { "cubo", cubo}
        });
    }

    /// <summary>
    /// Crea evento para la seleccion de dificultad
    /// </summary>
    /// <param name="dificultad"></param>
    public static void SeleccionDificultad(int dificultad)
    {
        Analytics.CustomEvent("seleccionDificultad", new Dictionary<string, object>
        {
            { "dificultad", dificultad}
        });
    }

    /// <summary>
    /// Crea evento para la seleccion de cubo
    /// </summary>
    /// <param name="cubo"></param>
    public static void SeleccionCubo(int cubo)
    {
        Analytics.CustomEvent("seleccionCubo", new Dictionary<string, object>
        {
            { "cubo", cubo}
        });
    }

    /// <summary>
    /// Crea evento para la seleccion de modo de juego
    /// </summary>
    /// <param name="modo"></param>
    public static void SeleccionModo(int modo)
    {
        Analytics.CustomEvent("seleccionModo", new Dictionary<string, object>
        {
            { "modoJuego", modo}
        });
    }

    /// <summary>
    /// Crea el evento para el fin de partida.
    /// </summary>
    /// <param name="dificultad"></param>
    /// <param name="tiempo"></param>
    /// <param name="movimientos"></param>
    /// <param name="hayRecord"> Si el jugador vence su record anterior, debe ser TRUE </param>
    public static void FinPartida(int modo, int dificultad, int cubo, float tiempo, int movimientos, bool hayRecord, bool ganoLaPartida)
    {
        string nombreEvento = "finPartida";
        if (hayRecord)
            nombreEvento += "ConRecord";
        else
            nombreEvento += "SinRecord";

        Analytics.CustomEvent(nombreEvento, new Dictionary<string, object>
        {
            { "dificultad", dificultad},
            { "modoJuego", modo},
            { "cubo", cubo},
            { "tiempo", tiempo},
            { "gano", ganoLaPartida},
            { "movimientos", movimientos}
        });
    }

    /// <summary>
    /// Crea el evento cuando el jugador decide volver a jugar la partida.
    /// </summary>
    /// <param name="dificultad"></param>
    /// <param name="tiempo"></param>
    /// <param name="movimientos"></param>
    public static void ResetPartida(int modo, int dificultad, int cubo, float tiempo, int movimientos)
    {
        Analytics.CustomEvent("resetPartida", new Dictionary<string, object>
        {
            { "dificultad", dificultad},
            { "modoJuego", modo},
            { "cubo", cubo},
            { "tiempo", tiempo},
            { "movimientos", movimientos}
        });
    }

    /// <summary>
    /// Crea el evento cuando el jugador decide terminar su partida acutal y comenzar otra.
    /// </summary>
    /// <param name="dificultad"></param>
    /// <param name="tiempo"></param>
    /// <param name="movimientos"></param>
    public static void ReplayPartida(int modo, int dificultad, int cubo, float tiempo, int movimientos)
    {
        Analytics.CustomEvent("replayPartida", new Dictionary<string, object>
        {
            { "dificultad", dificultad},
            { "modoJuego", modo},
            { "cubo", cubo},
            { "tiempo", tiempo},
            { "movimientos", movimientos}
        });
    }

    /// <summary>
    /// Crea el evento cuando el jugador se va al menu principal.
    /// </summary>
    /// <param name="dificultad"></param>
    /// <param name="tiempo"></param>
    /// <param name="movimientos"></param>
    public static void ExitPartida(int modo, int dificultad, int cubo, float tiempo, int movimientos)
    {
        Analytics.CustomEvent("exitPartida", new Dictionary<string, object>
        {
            { "dificultad", dificultad},
            { "modoJuego", modo},
            { "cubo", cubo},
            { "tiempo", tiempo},
            { "movimientos", movimientos}
        });
    }

    /// <summary>
    /// Evento de inicio del tutorial
    /// </summary>
    public static void InicioTutorial()
    {
        Analytics.CustomEvent("inicioTutorial", new Dictionary<string, object>
        {});
    }

    /// <summary>
    /// Evento de siguiente paso del tutorial
    /// </summary>
    public static void SiguienteTutorial()
    {
        Analytics.CustomEvent("siguienteTutorial", new Dictionary<string, object>
        { });
    }

    /// <summary>
    /// Evento de fin del tutorial
    /// </summary>
    public static void FinTutorial()
    {
        Analytics.CustomEvent("finTutorial", new Dictionary<string, object>
        { });
    }
}
