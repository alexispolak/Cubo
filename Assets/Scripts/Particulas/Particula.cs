﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particula : MonoBehaviour
{
    [SerializeField]
    private Transform[] waypoints;

    [SerializeField]
    [Range(0,100)]
    private float velocidad;

    #region PROPIEDADES
    public Transform[] Waypoints
    {
        get
        {
            return waypoints;
        }

        set
        {
            waypoints = value;
        }
    }
    public float Velocidad
    {
        get
        {
            return velocidad;
        }

        set
        {
            velocidad = value;
        }
    }

    #endregion

    void Start()
    {
        this.AcomodarWaypoints();
    }

    private void AcomodarWaypoints()
    {
        var variacion = 0f;
        var delta = (this.Waypoints[this.Waypoints.Length-1].position.y - this.Waypoints[0].position.y) /this.Waypoints.Length;
        foreach (Transform unWP in this.Waypoints)
        {
            unWP.position = new Vector3(unWP.position.x, unWP.position.y + variacion, unWP.position.z);
            variacion += delta;
        }
    }

    public void IniciarParticulas()
    {
        this.StartCoroutine(this.MovimientoParticulas(Waypoints, 0));
    }

    private IEnumerator MovimientoParticulas(Transform[] destino, int indice)
    {
        while (this.transform.position != destino[indice].position)
        {
            this.transform.position = Vector3.MoveTowards(this.transform.position, destino[indice].position, this.Velocidad * Time.deltaTime);
            yield return null;         
        }

        if (indice + 1 <= this.Waypoints.Length - 2)
            this.StartCoroutine(this.MovimientoParticulas(Waypoints, indice + 1));
        else
            this.transform.position = Waypoints[0].position;
    }
}
