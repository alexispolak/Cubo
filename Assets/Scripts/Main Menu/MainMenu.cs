﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    #region ATRIBUTOS
    [SerializeField]
    private Animator animadorMainMenu;

    [SerializeField]
    private Animator animadorGameModesSelection;

    [SerializeField]
    private Animator animadorDifficultySelection;

    [SerializeField]
    private Animator animadorScoreboard;

    [SerializeField]
    private Animator animadorCubeSelection;

    [SerializeField]
    private Text textoScore;
    #endregion

    #region PROPIEDADES
    private string Score { set; get; }
    public Text TextoScore
    {
        get
        {
            return textoScore;
        }

        set
        {
            textoScore = value;
        }
    }

    private int ModoJuegoSeleccionado { set; get; }
    private int DificultadSeleccionada { set; get; }
    private int CuboSeleccionado { set; get;
    }
    //Animadores
    public Animator AnimadorMainMenu
    {
        get
        {
            return animadorMainMenu;
        }

        set
        {
            animadorMainMenu = value;
        }
    }
    public Animator AnimadorGameModesSelection
    {
        get
        {
            return animadorGameModesSelection;
        }

        set
        {
            animadorGameModesSelection = value;
        }
    }
    public Animator AnimadorDifficultySelection
    {
        get
        {
            return animadorDifficultySelection;
        }

        set
        {
            animadorDifficultySelection = value;
        }
    }
    public Animator AnimadorScoreboard
    {
        get
        {
            return animadorScoreboard;
        }

        set
        {
            animadorScoreboard = value;
        }
    }
    public Animator AnimadorCubeSelection
    {
        get
        {
            return animadorCubeSelection;
        }

        set
        {
            animadorCubeSelection = value;
        }
    }

    #endregion

    #region METODOS UNITY

    void Awake()
    {
        this.Score = PlayerPrefs.GetInt("Movimientos", 0) + " - " + this.FormatearTiempoPartida(PlayerPrefs.GetFloat("Tiempo", 0));
        this.TextoScore.text = this.Score;
    }

    // Update is called once per frame
    void Start()
    {
        this.MostrarMainMenu();
    }
    #endregion


    #region METODOS
    #region BOTONES
    public void Play()
    {
        this.EsconderMainMenu();
        this.MostrarGameModes();
    }

    public void SeleccionModoJuego(int modo)
    {
        this.ModoJuegoSeleccionado = modo;

        Analisis.SeleccionModo(modo);

        this.OcultarGameModes();
        this.MostrarDificultades();
    }

    public void AbrirSeleccionCubos()
    {

    }

    public void SeleccionarCubo(int numeroCubo)
    {
        this.CuboSeleccionado = numeroCubo;

        Analisis.SeleccionCubo(numeroCubo);

        this.MostrarGameModes();
    }

    public void VerScoreboard()
    {

    }

    /// <summary>
    /// Da comienzo a la scene del juego
    /// </summary>
    /// <param name="dificultad"></param>
    public void ComenzarJuego(int dificultad)
    {
        this.DificultadSeleccionada = dificultad;

        Analisis.SeleccionDificultad(dificultad);
        
        this.SetearPreferenciasUsuario();

        this.IniciarPartida();
        
    }

    public void GSBack()
    {
        this.OcultarGameModes();
        this.MostrarMainMenu();
    }

    public void DBack()
    {
        this.OcultarDificultades();
        this.MostrarGameModes();
    }
    #endregion

    /// <summary>
    /// Guarda las preferencias del usuario
    /// </summary>
    private void SetearPreferenciasUsuario()
    {
        PlayerPrefs.SetInt("ModoJuego", this.ModoJuegoSeleccionado);
        PlayerPrefs.SetInt("Dificultad", this.DificultadSeleccionada);
        PlayerPrefs.SetInt("Cubo", this.CuboSeleccionado);
    }

    /// <summary>
    /// Inicia la scene correspondiente segun el modo de juego seleccionado
    /// </summary>
    private void IniciarPartida()
    {
        Analisis.InicioPartida(this.DificultadSeleccionada, this.ModoJuegoSeleccionado, this.CuboSeleccionado);

        var sceneParaCargar = "ModoJuego-" + this.ModoJuegoSeleccionado;
        SceneManager.LoadScene(sceneParaCargar);
    }

    /// <summary>
    /// Muestra la pantalla de seleccion de modos de juego
    /// </summary>
    private void MostrarGameModes()
    {
        this.AnimadorGameModesSelection.gameObject.SetActive(true);
        this.AnimadorGameModesSelection.SetBool("hide", false);       
        this.AnimadorGameModesSelection.Play("show");
    }

    /// <summary>
    /// Oculta la pantalla de seleccion de dificultades
    /// </summary>
    private void OcultarGameModes()
    {
        this.AnimadorGameModesSelection.SetBool("hide", true);
        this.AnimadorGameModesSelection.gameObject.SetActive(false);
    }

    /// <summary>
    /// Muestra las distintas opciones de dificultad de partida
    /// </summary>
    private void MostrarDificultades()
    {
        this.AnimadorDifficultySelection.gameObject.SetActive(true);
        this.AnimadorDifficultySelection.SetBool("hide", false);     
        this.AnimadorDifficultySelection.Play("show");
    }

    /// <summary>
    /// Oculta la pantalla de seleccion de dificultades
    /// </summary>
    private void OcultarDificultades()
    {
        this.AnimadorDifficultySelection.SetBool("hide", true);
        this.AnimadorDifficultySelection.gameObject.SetActive(false);
    }

    /// <summary>
    /// Setea el parametro para que comiencen las animaciones para esconder la GUI
    /// </summary>
    private void EsconderMainMenu()
    {
        this.AnimadorMainMenu.SetBool("hide", true);
        this.AnimadorMainMenu.gameObject.SetActive(true);
    }

    /// <summary>
    /// Muestra la pantalla de main menu
    /// </summary>
    private void MostrarMainMenu()
    {   
        this.AnimadorMainMenu.gameObject.SetActive(true);
        this.AnimadorMainMenu.SetBool("hide", false);
        this.AnimadorMainMenu.Play("show");
    }

    /// <summary>
    /// Calcula el tiempo en minutos:segundos
    /// </summary>
    /// <returns></returns>
    private string FormatearTiempoPartida(float tiempo)
    {
        string minutos = Mathf.Floor(tiempo / 60).ToString("00");
        string segundos = Mathf.Floor(tiempo % 60).ToString("00");

        return minutos + ":" + segundos;
    }
    #endregion
}
