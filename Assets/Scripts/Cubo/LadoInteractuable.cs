﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadoInteractuable : MonoBehaviour {

    #region ATRIBUTOS
    [SerializeField]
    private bool debugNormales;
    #endregion

    #region PROPIEDADES
    private Rotador PadreActual { set; get; }
    private Rotador PadreOriginal { set; get; }
    private float VelocidadRotacion { set; get; }
    private float SensibilidadRotacion { set; get; }

    private CuboMagico CuboMagico { set; get; }
    public Vector3 Normal { private set; get; }

    private int AnguloInicial { set; get; }
    public bool PuedoInteractuar { set; get; }

    //DEBUG
    private Material Rojo { set; get; }
    private Material Azul { set; get; }
    private bool ActivarDebugLados { set; get; }
    #endregion

    #region METODOS UNITY
    // Use this for initialization
    void Awake()
    {
        this.VelocidadRotacion = 100f;
        this.SensibilidadRotacion = 10f;

        this.CuboMagico = GameObject.FindGameObjectWithTag("cuboMagico").GetComponent<CuboMagico>();
        this.Normal = this.transform.TransformDirection(this.GetComponent<MeshFilter>().mesh.normals[0]);

        this.Rojo = GameObject.Find("ROJO").GetComponent<MeshRenderer>().material;
        this.Azul = GameObject.Find("AZUL").GetComponent<MeshRenderer>().material;
    }

    private void Start()
    {
        this.debugNormal();
        this.ObtenerPadreActual();
        this.PadreOriginal = this.PadreActual;
    }
    #endregion

    #region Mientras Arrastro
    /// <summary>
    /// Mientras arrastro.
    /// Hago que el padre gire continuamente segun el movimiento del mouse.
    /// </summary>
    private void OnMouseDrag()
    {
        if(this.PuedoInteractuar && this.PresionoLadoValido())
        {
            var y = Input.GetAxis("Mouse Y") * this.VelocidadRotacion * Mathf.Deg2Rad;
            var x = Input.GetAxis("Mouse X") * this.VelocidadRotacion * Mathf.Deg2Rad;

            this.PadreActual.GiroContinuo(this.CalcularMovimiento(x, y));
        }
    }

    /// <summary>
    /// Determina los signos del input dependiendo del angulo con el padre.
    /// </summary>
    /// <returns> el promedio de los imputs</returns>
    private float CalcularMovimiento(float x, float y)
    {
        switch (this.AnguloInicial)
        {
            case 135:
                y *= -1;
                break;
            case -180:
                y *= -1;
                break;
            case -135:
                y *= -1;
                x *= -1;
                break;
            case -90:
                x *= -1;
                break;
            case -45:
                x *= -1;
                break;        
        }
        return (x + y) /2;
    }

    /// <summary>
    /// Cuando presiono sobre un lado. 
    /// Obtengo el padre de ese lado, actualizo sus cubos y los asigno como hijos.
    /// </summary>
    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0) && this.PuedoInteractuar && this.PresionoLadoValido())
        {
            this.ObtenerPadreActual();

            this.PrepararPadre();

            this.CalcularAngulo();
        }
    }

    /// <summary>
    /// Chequea que se este presionando uno de los lados que se ve.
    /// </summary>
    /// <returns></returns>
    private bool PresionoLadoValido()
    {
        this.ActualizarNormal();
        if (this.Normal == new Vector3(0, 1, 0))
            return true;
        if (this.Normal == new Vector3(1, 0, 0))
            return true;
        if (this.Normal == new Vector3(0, 0, -1))
            return true;

        return false;
    }

    /// <summary>
    /// Realiza el calculo del angulo dependiendo de la normal
    /// </summary>
    private void CalcularAngulo()
    {
        var anguloRad = 0f;  
        var vector = this.PadreActual.transform.position - this.transform.position;

        if (this.Normal == new Vector3(0, 0, -1))
            anguloRad = Mathf.Atan2(-vector.y,vector.x);
        else if (this.Normal == new Vector3(0, 1, 0))
            anguloRad = Mathf.Atan2(-vector.z, vector.x);
        else if (this.Normal == new Vector3(1, 0, 0))
            anguloRad = Mathf.Atan2(-vector.y, vector.z);

        this.AnguloInicial = Mathf.RoundToInt((anguloRad*180)/Mathf.PI);
    }
    #endregion

    #region Cuando Suelto

    /// <summary>
    /// Cuando suelto del dragg.
    /// Realizo el movimiento automatico (snap) segun lo quede girar.
    /// Para esto obtengo el padre del lado, actualizo sus cubos y le indico la cantidad que le queda girar de forma atuomatica.
    /// </summary>
    private void OnMouseUp()
    {
        if(this.PuedoInteractuar)
        { 
            this.PrepararPadre();

            this.RealizarRotacionAutomatica();

            this.PadreActual.ComplementoParaGiroAutomatico = 0;

            this.debugNormales = false;
        }
    }

    /// <summary>
    /// Preparo todos los datos del Padre del lado para la rotacion
    /// </summary>
    private void PrepararPadre()
    {
        this.PadreActual.MeEstanArrastrando = false;
        
        this.PadreActual.ActualizarEjeRotacion();
        this.CuboMagico.ActualizarListaCubosPara(this.PadreActual);
        this.PadreActual.ActualizarHijos();
    }

    /// <summary>
    /// Obtengo el padre actual segun su posicion.
    /// </summary>
    public void ObtenerPadreActual()
    {
        this.ActualizarNormal();

        //Obtengo el padre segun la normal del mesh
        this.PadreActual = this.CuboMagico.ObtenerCuboRotadorPadre(this.Normal);
    }

    /// <summary>
    /// Obtiene el vector normal en la posicion actual
    /// </summary>
    public void ActualizarNormal()
    {
        this.Normal = this.transform.TransformDirection(this.GetComponent<MeshFilter>().mesh.normals[0]).normalized;
    }

    /// <summary>
    /// Realiza el giro automatico hasta completar los 90 grados o el complemento.
    /// Tiene que pasar la sensibilidad para realizar la rotacion, sino vuelve a la posicion anterior.
    /// </summary>
    public void RealizarRotacionAutomatica()
    {
        var complemento = this.PadreActual.ComplementoParaGiroAutomatico;
        if (Mathf.Abs(complemento) > this.SensibilidadRotacion )
        {
            this.PadreActual.GiroCompleto = true;
            if (complemento > 0)
                this.PadreActual.RotarHacia(1, (90 - this.PadreActual.ComplementoParaGiroAutomatico));
            else
                this.PadreActual.RotarHacia(-1, (90 + this.PadreActual.ComplementoParaGiroAutomatico));
        }
        else
        {
            this.PadreActual.GiroCompleto = false;
            if (complemento > 0)
                this.PadreActual.RotarHacia(-1, (this.PadreActual.ComplementoParaGiroAutomatico));
            else
                this.PadreActual.RotarHacia(1, (-this.PadreActual.ComplementoParaGiroAutomatico));
        }
    }

    internal void ActivarDebug()
    {
        this.ActivarDebugLados = true;
    }

    /// <summary>
    /// Chequea que el lado este mirando al padre que corresponde para ganar la partida.
    /// TODO podría sacar el Debug, y quedaria mucho mas limpia y no necesitaría hacer el segundo if, directamente devuelvo el resultado.
    /// </summary>
    /// <returns></returns>
    public bool EstaEnPosicionCorrecta()
    {
        var origen = this.transform.position;
        var direccion = this.transform.TransformDirection(this.GetComponent<MeshFilter>().mesh.normals[0]);
        RaycastHit hit;
        var layerMask = 1 << 8;
        if (Physics.Raycast(this.transform.position, direccion, out hit, Mathf.Infinity, layerMask))
        {
            this.DebugLados(origen, direccion);
            if (hit.transform.gameObject == this.PadreOriginal.TriggerGanador)
            {
                if(this.ActivarDebugLados)
                    this.GetComponent<LineRenderer>().material = this.Azul;

                return true;
            }
        }
        return false;
    }

    private void DebugLados(Vector3 origen, Vector3 direccion)
    {
        if(this.ActivarDebugLados)
        { 
            this.GetComponent<LineRenderer>().SetPosition(0, origen);
            this.GetComponent<LineRenderer>().SetPosition(1, (direccion * 10));
            this.GetComponent<LineRenderer>().startWidth = .1f;
            this.GetComponent<LineRenderer>().endWidth = .1f;
            this.GetComponent<LineRenderer>().material = this.Rojo;
        }
    }

    public void debugNormal()
    {
        if(debugNormales)
        { 
            //Establezco la posicion incial del raycast
            var posicionInicial = this.transform.position;
            var posicionFinal = (this.transform.TransformDirection(this.GetComponent<MeshFilter>().mesh.normals[0]) * 9) + posicionInicial;

            this.GetComponent<LineRenderer>().SetPosition(0, posicionInicial);
            this.GetComponent<LineRenderer>().SetPosition(1, posicionFinal);
            this.GetComponent<LineRenderer>().startWidth = .1f;
            this.GetComponent<LineRenderer>().endWidth = .1f;
            this.GetComponent<LineRenderer>().material=this.GetComponent<MeshRenderer>().material;
        }

    }
    #endregion
}
