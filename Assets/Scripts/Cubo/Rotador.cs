﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotador : MonoBehaviour
{
    #region ATRIBUTOS
    [SerializeField]
    private GameObject ladoRotador;
    [SerializeField]
    private GameObject triggerGanador;
    #endregion

    #region PROPIEDADES
    private CuboMagico CuboMagico { set; get; }
    public List<GameObject> ListaCubos { set; get; }
    public Vector3 EjeRotacion { private set; get; }
    public bool MeEstanArrastrando { set; get; }
    private float CantidadGiroContinuo { set; get; }
    public float ComplementoParaGiroAutomatico { set; get; }
    public bool GiroCompleto { set; get; }

    private bool TengoQueRotar { set; get; }
    private int DireccionRotacion { set; get; }
    private float QuedaRotar { set; get; }
    private float VelocidadRotacion { set; get; }

    private GameObject LadoRotador
    {
        get
        {
            return ladoRotador;
        }

        set
        {
            ladoRotador = value;
        }
    }

    public GameObject TriggerGanador
    {
        get
        {
            return triggerGanador;
        }

        set
        {
            triggerGanador = value;
        }
    }

    #endregion

    #region METODOS UNITY
    // Use this for initialization
    void Awake()
    {
        this.ListaCubos = new List<GameObject>();
        this.CuboMagico = GameObject.FindGameObjectWithTag("cuboMagico").GetComponent<CuboMagico>();
        this.VelocidadRotacion = 5f;
        this.ActualizarEjeRotacion();
    }

    // Update is called once per frame
    void Update()
    {
        if (this.TengoQueRotar)
        {
            this.Rotar();
        }
        else if (this.MeEstanArrastrando)
        {
            this.Rotar(this.CantidadGiroContinuo);
        }

        //this.debugNormal();
    } 
    #endregion

    #region Rotacion
    /// <summary>
    /// Si se determina un lado de rotacion, la realiza.
    /// <param name="direccion"> puede ser 1 o -1</param>
    /// </summary>
    public void RotarHacia(int direccion, float quedaRotar)
    {
        if (this.CuboMagico.PermiteMovimiento)
        {
            this.CuboMagico.DenegarMovimiento();

            this.ActualizarHijos();

            StartCoroutine(this.RealizarRotacion(direccion, quedaRotar));
        }
    }

    /// <summary>
    /// Realiza la rotacion continua mientras lo arrastran
    /// </summary>
    public void GiroContinuo(float cantidadGiro)
    {
        this.MeEstanArrastrando = true;
        this.CantidadGiroContinuo = cantidadGiro;
    }

    /// <summary>
    /// Es el encargado de realizar la rotacion hasa que complete los 90°
    /// </summary>
    /// <returns></returns>
    private IEnumerator RealizarRotacion(int direccion, float quedaRotar)
    {

        this.TengoQueRotar = true;
        this.DireccionRotacion = direccion;
        this.QuedaRotar = quedaRotar;

        yield return new WaitUntil(() => this.QuedaRotar <= 0);

        if (this.GiroCompleto)
            this.CuboMagico.CompletoMovimiento();

        this.CuboMagico.ChequearGanador();
        this.TengoQueRotar = false;
        this.CuboMagico.PermitirMovimiento();

        this.VelocidadRotacion = 5f;
        this.DejarHijos();
    }

    /// <summary>
    /// Rota al objeto en cada frame segun la direccion, velocidad y eje.
    /// </summary>
    public void Rotar()
    {
        if (this.QuedaRotar - this.VelocidadRotacion < 0)
            this.VelocidadRotacion = this.QuedaRotar;

        this.transform.Rotate(this.EjeRotacion,this.VelocidadRotacion * this.DireccionRotacion,Space.World);

        this.QuedaRotar -= this.VelocidadRotacion;
    }

    /// <summary>
    /// Rota al objeto en cada frame segun la direccion, velocidad y eje.
    /// </summary>
    private void Rotar(float cantidadGiro)
    {
        if (this.CuboMagico.PermiteMovimiento)
        {
            this.transform.Rotate(this.EjeRotacion,cantidadGiro, Space.World);

            this.CalcularComplementoCon(cantidadGiro);
        }
    }

    /// <summary>
    /// Calcula lo que le falta para completar la rotacion
    /// </summary>
    private void CalcularComplementoCon(float cantidadGiro)
    {
        this.ComplementoParaGiroAutomatico += cantidadGiro;

        if (this.ComplementoParaGiroAutomatico > 0)
        {
            if (this.ComplementoParaGiroAutomatico > 90)
                this.ComplementoParaGiroAutomatico -= 90;
        }
        else
        {
            if (this.ComplementoParaGiroAutomatico < -90)
                this.ComplementoParaGiroAutomatico += 90;
        }
    }

    /// <summary>
    /// Devuelve la direccion de giro
    /// </summary>
    /// <param name="izk"> Click Izquierdo del Mouse</param>
    /// <param name="der"> Click Derecho del Mouse </param>
    /// <returns> 1 = Gira Izquierda, 2 = Gira Derecha. </returns>
    private int DeterminarDireccionGiro(bool izk, bool der)
    {
        if (izk)
            return 1;
        else if (der)
            return -1;
        else
            return 0;
    }
    #endregion

    /// <summary>
    /// Actualiza los hijos de este rotador segu
    /// </summary>
    public void ActualizarHijos()
    {
        this.DejarHijos();

        foreach (GameObject unCubo in this.ListaCubos)
        {
            unCubo.transform.SetParent(this.transform);
        }
    }

    /// <summary>
    /// Deja los hijos que tenia antes
    /// </summary>
    private void DejarHijos()
    {
        foreach (GameObject unCubo in this.ListaCubos)
        {
            unCubo.transform.SetParent(this.transform.parent);
        }
    }

    /// <summary>
    /// Setea la velocidad de giro para el desorden
    /// </summary>
    /// <param name="nuevaVelocidad"></param>
    public void SetearVelocidadDesorden(float nuevaVelocidad)
    {
        this.VelocidadRotacion = nuevaVelocidad;
    }

    /// <summary>
    /// Actualiza el eje de rotacion segun la normal.
    /// </summary>
    public void ActualizarEjeRotacion()
    {
        this.EjeRotacion = this.transform.TransformDirection(this.LadoRotador.GetComponent<MeshFilter>().mesh.normals[0].normalized);
    }

    private void debugNormal()
    {
        //Establezco la posicion incial del raycast
        var posicionInicial = this.transform.position;

        var posicionFinal = (this.EjeRotacion * 9) + posicionInicial;

        this.GetComponentInChildren<LineRenderer>().SetPosition(0, posicionInicial);
        this.GetComponentInChildren<LineRenderer>().SetPosition(1, posicionFinal);
        this.GetComponentInChildren<LineRenderer>().startWidth = .1f;
        this.GetComponentInChildren<LineRenderer>().endWidth = .1f;
        this.GetComponentInChildren<LineRenderer>().material = this.GetComponentInChildren<MeshRenderer>().material;
    }
}
