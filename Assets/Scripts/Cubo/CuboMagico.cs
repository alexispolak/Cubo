﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuboMagico : MonoBehaviour

{
    #region ATRIBUTOS
    [SerializeField]
    private Rotador[] listaCubosRotadores;

    [SerializeField]
    private bool activarDebugLados;
    #endregion

    #region PROPIEDADES
    private Partida Partida { set; get; }

    public List<LadoInteractuable> ListaLados { set; get; }
    public bool PermiteMovimiento { private set; get; }
    public int CantidadMovimientos { private set; get; }

    public Rotador[] ListaCubosRotadores
    {
        get
        {
            return listaCubosRotadores;
        }

        set
        {
            listaCubosRotadores = value;
        }
    }
    private GameObject[] CubosUnitarios { set; get; }

    private AnimacionCubo AnimacionCubo { set; get; }

    //Rotacion
    private bool TengoQueRotar { get; set; }
    private int DireccionRotacion { get; set; }
    private float QuedaRotar { get; set; }
    private float VelocidadRotacion { set; get; }
    public Vector3 EjeRotacion { get; private set; }

    #endregion

    #region METODOS UNITY
    // Use this for initialization
    void Awake()
    {
        this.ListaLados = new List<LadoInteractuable>();
        this.CubosUnitarios = GameObject.FindGameObjectsWithTag("cuboUnitario");
        this.AnimacionCubo = this.GetComponent<AnimacionCubo>();   
        this.Partida = GameObject.FindGameObjectWithTag("partida").GetComponent<Partida>();
        this.PermiteMovimiento = true;
        this.VelocidadRotacion = 5;
        this.DesactivarVista();
        this.CantidadMovimientos = 0;
    }

    void Start()
    {
        this.ObtenerLados();

        if (this.activarDebugLados)
            this.ActivarDebugLados();
    }


    // Update is called once per frame
    void Update()
    {
        this.DetectarRotacionCubo();

        if (this.TengoQueRotar)
        {
            this.Rotar();
        }
    }
    #endregion

    #region METODOS
    public void AnimacionMostrarse()
    {
        this.ActivarVista();
        this.AnimacionCubo.Mostrarse(this.CubosUnitarios);
    }

    private void DesactivarVista()
    {
        foreach (GameObject unCubo in this.CubosUnitarios)
        {
            foreach (MeshRenderer unLado in unCubo.GetComponentsInChildren<MeshRenderer>())
            {
                unLado.enabled = false;
            }
        }
    }

    private void ActivarVista()
    {
        foreach (GameObject unCubo in this.CubosUnitarios)
        {
            foreach (MeshRenderer unLado in unCubo.GetComponentsInChildren<MeshRenderer>())
            {
                unLado.enabled = true;
            }
        }
    }

    public void AnimacionOcultarse()
    {
        this.AnimacionCubo.Ocultarse(this.CubosUnitarios);
    }

    /// <summary>
    /// Habilita/Deshabilita la interaccion del jugador con los lados del cubo.
    /// </summary>
    public void InteraccionConLados(bool valor)
    {
        foreach (LadoInteractuable lado in this.ListaLados)
        {
            lado.PuedoInteractuar = valor;
        }
    }

    /// <summary>
    /// Devuelve el CuboRotador que tenga la misma normal.
    /// </summary>
    /// <param name="normal"></param>
    /// <returns></returns>
    public Rotador ObtenerCuboRotadorPadre(Vector3 normal)
    {
        foreach (Rotador unCuboRotador in this.ListaCubosRotadores)
        {
            if (unCuboRotador.EjeRotacion == normal)
            {
                return unCuboRotador;
            }
        }
        Debug.Log("No deberia entrar aca: " + normal);
        return null;
    }
    /// <summary>
    /// Actualiza los hijos de un cubo de rotacion.
    /// </summary>
    /// <param name="cuboRotador">El cubo rotador que tiene que actualizar los hijos</param>
    public void ActualizarListaCubosPara(Rotador cuboRotador)
    {
        cuboRotador.ListaCubos = new List<GameObject>();

        foreach (LadoInteractuable unLado in this.ListaLados)
        {
            unLado.ActualizarNormal();

            if (unLado.Normal == cuboRotador.EjeRotacion)
            {
                cuboRotador.ListaCubos.Add(unLado.transform.parent.gameObject);
            }
        }
    }

    /// <summary>
    /// Actualiza las normales de los lados
    /// </summary>
    public void ActualizarNormalesLados()
    {
        foreach (LadoInteractuable unLado in this.ListaLados)
        {
            unLado.ActualizarNormal();
        }
    }

    /// <summary>
    /// Obtengo solamente los lados interactuables.
    /// </summary>
    private void ObtenerLados()
    {
        var listaSinFiltro = GameObject.FindGameObjectsWithTag("lado");
        foreach (GameObject unLado in listaSinFiltro)
        {
            if (unLado.GetComponent<LadoInteractuable>() != null)
                this.ListaLados.Add(unLado.GetComponent<LadoInteractuable>());
        }
    }

    /// <summary>
    /// Avisa al CuboMagico que puede volver a realizarse un movimiento de rotacion.
    /// </summary>
    public void PermitirMovimiento()
    {
        this.PermiteMovimiento = true;
    }

    /// <summary>
    /// Avisa al CuboMagico que se esta por rotar.
    /// </summary>
    public void DenegarMovimiento()
    {
        this.PermiteMovimiento = false;
    }

    /// <summary>
    /// Chequea si todos los lados estan mirando en la posicion ganadora
    /// </summary>
    public void ChequearGanador()
    {
        this.Partida.ChequearFinPartida(this.ChequeoPosicionGanadora());
    }

    /// <summary>
    /// Chequea si todos los lados estan mirando en la posicion ganadora
    /// </summary>
    public bool ChequeoPosicionGanadora()
    {
        bool condicionParaGanar = true;

        foreach(LadoInteractuable lado in this.ListaLados)
        {
            if (!lado.EstaEnPosicionCorrecta())
            {
                condicionParaGanar = false;
            }
        }

        if (condicionParaGanar)
            this.InteraccionConLados(false);

        return condicionParaGanar;
    }

    #endregion

    #region ROTACION
    /// <summary>
    /// Detecta el Input para rotar el cubo entero 
    /// </summary>
    private void DetectarRotacionCubo()
    {
        var x = Input.GetAxis("Horizontal");
        var y = Input.GetAxis("Vertical");

        if(this.PermiteMovimiento)
            this.RotarCuboCompleto(-x, y);
    }

    /// <summary>
    /// Rota al cubo en el eje que corresponde.
    /// </summary>
    private void RotarCuboCompleto(float horizontal, float vertical)
    {
        var direccionHorizontal = (horizontal > 0) ? 1:-1;
        var direccionVertical = (vertical > 0) ? 1 : -1;

        if(!this.TengoQueRotar)
        { 
            if (horizontal != 0)
                StartCoroutine(RealizarRotacion(direccionHorizontal, Vector3.up, 90));

            if(vertical != 0)
                StartCoroutine(RealizarRotacion(direccionVertical, Vector3.right, 90));
        }
    }

    /// <summary>
    /// Es el encargado de realizar la rotacion hasa que complete los 90°
    /// </summary>
    /// <returns></returns>
    private IEnumerator RealizarRotacion(int direccion, Vector3 eje, float quedaRotar)
    {
        this.TengoQueRotar = true;
        this.DireccionRotacion = direccion;
        this.QuedaRotar = quedaRotar;
        this.EjeRotacion = eje;

        yield return new WaitUntil(() => this.QuedaRotar <= 0);

        this.ChequearGanador();
        this.TengoQueRotar = false;
        this.ActualizarEjesRotacion();
        this.ActualizarNormalesLados();
    }

    private void ActualizarEjesRotacion()
    {
        foreach (Rotador cuboRotador in this.ListaCubosRotadores)
        {
            cuboRotador.ActualizarEjeRotacion();
        }
    }

    private void ActivarDebugLados()
    {
        foreach (LadoInteractuable lado in this.ListaLados)
        {
            lado.ActivarDebug();
        }
    }

    /// <summary>
    /// Rota al objeto en cada frame segun la direccion, velocidad y eje.
    /// </summary>
    public void Rotar()
    {
        if (this.QuedaRotar - this.VelocidadRotacion < 0)
            this.VelocidadRotacion = this.QuedaRotar;

        this.transform.Rotate(this.EjeRotacion, this.VelocidadRotacion * this.DireccionRotacion, Space.World);
        this.QuedaRotar -= this.VelocidadRotacion;
    }

    /// <summary>
    /// Se llama cada vez que se completa un movimiento de piezas
    /// es decir, cuando termina de girar un rotador
    /// </summary>
    public void CompletoMovimiento()
    {
        this.CantidadMovimientos += 1;
    }
    #endregion
}
