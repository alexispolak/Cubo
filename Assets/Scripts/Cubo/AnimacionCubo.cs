﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimacionCubo : MonoBehaviour
{
    public void Mostrarse(GameObject[] listaCubos)
    {
        foreach (GameObject unCubo in listaCubos)
        {
            StartCoroutine(this.ReuinirseEnElCentro(unCubo, unCubo.transform.position, 25));
        }  
    }

    private IEnumerator ReuinirseEnElCentro(GameObject unCubo, Vector3 destino, float velocidad)
    {
        unCubo.transform.position = UnityEngine.Random.onUnitSphere * 25;
        while (unCubo.transform.position != destino)
        {
            unCubo.transform.position = Vector3.MoveTowards(unCubo.transform.position, destino, velocidad * Time.deltaTime);
            yield return null;
        }
    }

    public void Ocultarse(GameObject[] listaCubos)
    {
        foreach (GameObject unCubo in listaCubos)
        {
            StartCoroutine(this.SalirDeLaPantalla(unCubo, UnityEngine.Random.onUnitSphere * 0.01f, 10));
        }
    }

    private IEnumerator SalirDeLaPantalla(GameObject unCubo, Vector3 destino, float velocidad)
    {
        while (unCubo.transform.position != destino)
        {
            unCubo.transform.position = Vector3.MoveTowards(unCubo.transform.position, destino, velocidad * Time.deltaTime);
            yield return null;
        }
    }
}
