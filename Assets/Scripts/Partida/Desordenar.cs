﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Desordenar : MonoBehaviour
{

    #region ATRIBUTOS
    private const float cantidadRotacion = 90f;
    private int[] direccionGiro = { -1, 1 };
    #endregion

    #region PROPIEDADES
    public int CantidadDesorden { set; get; }
    private Partida Partida { set; get; }
    #endregion

    #region METODOS
    public void ComenzarDesorden(int cantidadVeces, CuboMagico cuboADesordenar, Partida partida)
    {
        this.Partida = partida;
        StartCoroutine(RealizarDesorden(cantidadVeces, cuboADesordenar));
    }

    public void ComenzarDesorden(CuboMagico cuboADesordenar, Partida partida, Rotador cuboRotador, int direccion)
    {
        this.Partida = partida;
        StartCoroutine(RealizarDesorden(cuboADesordenar, cuboRotador, direccion));
    }

    /// <summary>
    /// Corrutina que se encarga de realizar el desorden del cubo que recibe por parametro
    /// </summary>
    private IEnumerator RealizarDesorden(CuboMagico cuboADesordenar, Rotador cuboRotador, int direccion)
    {
            this.PrepararCuboMagico(cuboRotador, cuboADesordenar);
            cuboRotador.RotarHacia(this.direccionGiro[direccion], cantidadRotacion);

            yield return new WaitUntil(() => cuboADesordenar.PermiteMovimiento);

            this.Partida.TerminoDesorden();
    }

    /// <summary>
    /// Corrutina que se encarga de realizar el desorden del cubo que recibe por parametro
    /// </summary>
    private IEnumerator RealizarDesorden(int cantidadVeces, CuboMagico cuboADesordenar)
    {
        if (cantidadVeces > 0)
        {
            var cuboRandom = UnityEngine.Random.Range(0, 5);
            var direccionRandom = UnityEngine.Random.Range(0, 1);

            Rotador cuboRotador = cuboADesordenar.ListaCubosRotadores[cuboRandom];
            this.PrepararCuboMagico(cuboRotador, cuboADesordenar);
            cuboRotador.RotarHacia(this.direccionGiro[direccionRandom], cantidadRotacion);

            yield return new WaitUntil(() => cuboADesordenar.PermiteMovimiento);

            StartCoroutine(RealizarDesorden(cantidadVeces - 1, cuboADesordenar));
        }
        else
        {
            this.Partida.TerminoDesorden();
        }
    }

    private void PrepararCuboMagico(Rotador cuboRotador, CuboMagico cuboADesordenar)
    {
        cuboADesordenar.ActualizarListaCubosPara(cuboRotador);
        cuboRotador.SetearVelocidadDesorden(90f);
    }
    #endregion
}
