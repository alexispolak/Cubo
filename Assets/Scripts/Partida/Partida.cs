﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Partida : MonoBehaviour
{
    #region ATRIBUTOS
    [SerializeField]
    private int cantidadDesorden;
    #endregion

    #region PROPIEDADES
    internal float TiempoPartida { private set; get; }
    internal bool HayPartidaEnCurso { get; set; }

    //COMPONENTES
    private Desordenar Desordenar { set; get; }
    private CuboMagico CuboMagico { set; get; }
    private GuiManager GuiManager { set; get; }
    private Camera Camara { set; get; }
    private Particula EfectosParticula { set; get; }


    public int CantidadDesorden
    {
        get
        {
            return cantidadDesorden;
        }

        set
        {
            cantidadDesorden = value;
        }
    }
    public int CuboSeleccionado { set; get; }
    public int ModoJuegoSeleccionado { set; get; }
    internal float TiempoLimite { set; get; }
    internal int CantidadMovimientosLimite { set; get; }

    #endregion

    #region METODOS UNITY

    // Use this for initialization
    void Awake()
    {
        this.InicioConfiguracionPartida();
    }

    void Start()
    {
        this.InicializarPartida();
    }

    // Update is called once per frame
    void Update()
    {
        this.ActualizarReloj();
        this.ChequeoDerrota();
    }
    #endregion


    #region METODOS

    /// <summary>
    /// Establece la configuracion de la partida, obteniendo y seteando todos los datos y componentes necesarios.
    /// </summary>
    private void InicioConfiguracionPartida()
    {
        ///Busca las cosas que necesita
        this.EfectosParticula = GameObject.Find("Particulas").GetComponent<Particula>();
        this.Camara = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();    
        this.CuboMagico = GameObject.FindGameObjectWithTag("cuboMagico").GetComponent<CuboMagico>();

        //Obtiene los componentes necesarios
        this.Desordenar = this.GetComponent<Desordenar>();
        this.GuiManager = this.GetComponent<GuiManager>();
        
        //Obtiene los datos iniciales de la partida
        this.CantidadDesorden = PlayerPrefs.GetInt("Dificultad");
        this.ModoJuegoSeleccionado = PlayerPrefs.GetInt("ModoJuego");
        this.CuboSeleccionado = PlayerPrefs.GetInt("Cubo");

        //Configuro la partida segun el modo de juego
        this.ConfigurarSegunModoJuego();

        //Setea valores iniciales
        this.TiempoPartida = 0;
        this.HayPartidaEnCurso = false;
    }

    /// <summary>
    /// Configuro la partida segun los parametros recibidos de modo de juego
    /// </summary>
    private void ConfigurarSegunModoJuego()
    {
        if (this.ModoJuegoSeleccionado.Equals(1))
            this.ConfigurarPartidaTimeTrial();
        else if (this.ModoJuegoSeleccionado.Equals(2))
            this.ConfigurarPartidaPorPasos();
    }

    /// <summary>
    /// Configura la condicion inicial de una partida por pasos.
    /// </summary>
    private void ConfigurarPartidaPorPasos()
    {
        this.CantidadMovimientosLimite = 10;
    }

    /// <summary>
    /// Configura la condicion inicial de una partida por tiempo.
    /// </summary>
    private void ConfigurarPartidaTimeTrial()
    {
        if (this.CantidadDesorden.Equals(2))
            this.TiempoLimite = 15;
        else if (this.CantidadDesorden.Equals(4))
            this.TiempoLimite = 30;
        else if (this.CantidadDesorden.Equals(10))
            this.TiempoLimite = 120;
    }

    /// <summary>
    /// Inicializa la partida o el tutorial, segun corresponda.
    /// </summary>
    private void InicializarPartida()
    {
        if (this.CantidadDesorden.Equals(1))
        {
            Analisis.InicioTutorial();
            this.Pausar();
            StartCoroutine(this.ComenzarTutorial());
        }
        else
            this.Desordenar.ComenzarDesorden(this.CantidadDesorden, this.CuboMagico, this);
    }

    /// <summary>
    /// Cuando termina de desordenar, puede comenzar la partida
    /// </summary>
    internal void TerminoDesorden()
    {
        this.CuboMagico.AnimacionMostrarse();
        this.HayPartidaEnCurso = true;
        this.CuboMagico.ActualizarNormalesLados();
        this.CuboMagico.InteraccionConLados(true);
        this.EfectosParticula.IniciarParticulas();
    }

    /// <summary>
    /// Actualiza el tiempo del reloj
    /// </summary>
    private void ActualizarReloj()
    {
        if (this.HayPartidaEnCurso)
        {
            this.TiempoPartida += Time.deltaTime;
        }
    }

    /// <summary>
    /// Chequea si la partida tiene que terminar
    /// </summary>
    /// <param name="estaEnPosicionGanadora"></param>
    internal void ChequearFinPartida(bool estaEnPosicionGanadora)
    {
        this.ChequeoVictoria(estaEnPosicionGanadora);

        this.ChequeoDerrota();
    }

    /// <summary>
    /// Chequea si la partida termina porque no se cumplieron las condiciones del modo de juego
    /// </summary>
    private void ChequeoDerrota()
    {
        if (this.ModoJuegoSeleccionado.Equals(1))
            this.ChequeoDerrotaPorTiempo();
        else if (this.ModoJuegoSeleccionado.Equals(2))
            this.ChequeoDerrotaPorPasos();
    }

    /// <summary>
    /// Chequea la condicion de derrota por tiempo limite
    /// </summary>
    private void ChequeoDerrotaPorTiempo()
    {
        if ((this.TiempoLimite - this.TiempoPartida) <= 0)
            this.TerminarPorDerrota("Time's Up!");
    }

    /// <summary>
    /// Chequea la condicion de derrota por cantidad de movimientos limite
    /// </summary>
    private void ChequeoDerrotaPorPasos()
    {
        if ((this.CantidadMovimientosLimite - this.CantidadMovimientosCubo()) <= 0)
            this.TerminarPorDerrota("You are out of moves");
    }

    /// <summary>
    /// Termina la partida por derrota
    /// </summary>
    /// <param name="mensajeDerrota"></param>
    private void TerminarPorDerrota(string mensajeDerrota)
    {
        this.OcultarCuboMagico();
        this.GuiManager.ActivarMenuWinLose(false);
        this.HayPartidaEnCurso = false;
        Analisis.FinPartida(this.ModoJuegoSeleccionado, this.CantidadDesorden, this.CuboSeleccionado, this.TiempoPartida, this.CantidadMovimientosCubo() + 1, false, false);
    }

    /// <summary>
    /// Chequea si se gana la partoda
    /// </summary>
    public bool ChequeoVictoria(bool victoria)
    {
        if (victoria)
        {
            this.OcultarCuboMagico();
            this.GuiManager.ActivarMenuWinLose(victoria);
            this.HayPartidaEnCurso = false;
            this.ChequeoGuardado();
            return true;
        }

        return false;
    }

    /// <summary>
    /// Chequea si corresponde guardar los datos de la partida
    /// </summary>
    private void ChequeoGuardado()
    {
        var mejorTiempo = PlayerPrefs.GetFloat("Tiempo", -1);
        var movimientos = PlayerPrefs.GetInt("Movimientos", 0);

        if (mejorTiempo.Equals(-1) || mejorTiempo > this.TiempoPartida)
        {
            this.GuardarDatos();
        }
        else if (mejorTiempo.Equals(this.TiempoPartida) && movimientos > this.CantidadMovimientosCubo())
        {
            this.GuardarDatos();
        }
        else
        {
            Analisis.FinPartida(this.ModoJuegoSeleccionado, this.CantidadDesorden, this.CuboSeleccionado, this.TiempoPartida, this.CantidadMovimientosCubo()+1, false, true);
        }
    }

    /// <summary>
    /// Guarda los datos de la partida
    /// </summary>
    private void GuardarDatos()
    {
        Analisis.FinPartida(this.ModoJuegoSeleccionado, this.CantidadDesorden, this.CuboSeleccionado, this.TiempoPartida, this.CantidadMovimientosCubo()+1, true, true);
        PlayerPrefs.SetFloat("Tiempo", this.TiempoPartida);
        PlayerPrefs.SetInt("Movimientos", this.CantidadMovimientosCubo()+1);
    }

    /// <summary>
    /// Pausa el tiempo y los movimientos de la partida.
    /// </summary>
    internal void Pausar()
    {
        this.HayPartidaEnCurso = false;
        this.CuboMagico.InteraccionConLados(false);
    }

    /// <summary>
    /// Sale de la pausa
    /// </summary>
    internal void DesPausar()
    {
        this.HayPartidaEnCurso = true;
        this.CuboMagico.InteraccionConLados(true);
    }

    /// <summary>
    /// Reproduce la animacion para ocultar al cubo.
    /// </summary>
    internal void OcultarCuboMagico()
    {
        this.CuboMagico.AnimacionOcultarse();
    }

    /// <summary>
    /// Devuelve la cantidad de movimiento que se realizaron en el cubo magico
    /// </summary>
    /// <returns></returns>
    public int CantidadMovimientosCubo()
    {
        return this.CuboMagico.CantidadMovimientos;
    }

    #region BOTONES
    /// <summary>
    /// Cuando el jugador quiere terminar su partida y comenzar una nueva.
    /// </summary>
    internal void Restart()
    {
        Analisis.ReplayPartida(this.ModoJuegoSeleccionado, this.CantidadDesorden, this.CuboSeleccionado, this.TiempoPartida, this.CantidadMovimientosCubo() + 1);
        this.RecargarEscena();
    }

    /// <summary>
    /// Cuando el jugador quiere volver a jugar, luego de haber terminado una.
    /// </summary>
    internal void Resetear()
    {
        Analisis.ResetPartida(this.ModoJuegoSeleccionado, this.CantidadDesorden, this.CuboSeleccionado, this.TiempoPartida, this.CantidadMovimientosCubo() + 1);
        this.RecargarEscena();
    }

    /// <summary>
    /// Recarga la escena
    /// </summary>
    private void RecargarEscena()
    {
        SceneManager.LoadScene("ModoJuego-"+this.ModoJuegoSeleccionado);
    }

    /// <summary>
    /// Se va al menu principal
    /// </summary>
    internal void Exit()
    {
        Analisis.ExitPartida(this.ModoJuegoSeleccionado, this.CantidadDesorden, this.CuboSeleccionado, this.TiempoPartida, this.CantidadMovimientosCubo() + 1);
        SceneManager.LoadScene("MenuPrincipal");
    }

    #endregion

    #region TUTORIALES
    /// <summary>
    /// Comienza el tutorial de la partida
    /// </summary>
    /// <returns></returns>
    private IEnumerator ComenzarTutorial()
    {
        yield return new WaitForSeconds(1);
        this.MostrarTutorial();
        this.Desordenar.ComenzarDesorden(this.CuboMagico, this, GameObject.Find("SUR Rotador").GetComponent<Rotador>(), 1);
    }

    /// <summary>
    /// Muestra el tutorial en la pantalla.
    /// </summary>
    private void MostrarTutorial()
    {
        this.GuiManager.MostrarTutorial();
    }

    /// <summary>
    /// Boton para el siguiente paso del tutorial
    /// </summary>
    internal void SiguienteTutorial()
    {
        Analisis.SiguienteTutorial();
        this.GuiManager.SiguienteTutorial();
    }

    /// <summary>
    /// Boton para terminar el tutorial
    /// </summary>
    internal void TerminarTutorial()
    {
        Analisis.FinTutorial();
        this.GuiManager.TerminarTutorial();
        this.DesPausar();
    }
    #endregion

    #endregion
}
