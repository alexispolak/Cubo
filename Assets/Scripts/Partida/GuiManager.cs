﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuiManager : MonoBehaviour
{

    #region ATRIBUTOS
    [SerializeField]
    private Text guiTiempo;

    [SerializeField]
    private Text guiCantMovimiento;

    [SerializeField]
    private GameObject guiMenuPausa;

    [SerializeField]
    private GameObject guiMenuWinLose;

    [SerializeField]
    private Text guiWinLoseText;

    [SerializeField]
    private Text guiEndData;

    [SerializeField]
    private GameObject ingameGui;

    [Header("Tutorial")]

    [SerializeField]
    private GameObject guiPrimerTutorial;

    [SerializeField]
    private GameObject guiSegundoTutorial;

    [SerializeField]
    private GameObject particulas;
    #endregion

    #region PROPIEDADES
    private Partida Partida { set; get; }
    private Animator IngameGuiAnimator { set; get; }
    private Animator PrimerTutorialAnimator { set; get; }
    private Animator SegundoTutorialAnimator { set; get; }

    public Text GuiTiempo
    {
        get
        {
            return guiTiempo;
        }

        set
        {
            guiTiempo = value;
        }
    }
    public Text GuiCantMovimiento
    {
        get
        {
            return guiCantMovimiento;
        }

        set
        {
            guiCantMovimiento = value;
        }
    }
    public GameObject GuiMenuPausa
    {
        get
        {
            return guiMenuPausa;
        }

        set
        {
            guiMenuPausa = value;
        }
    }
    public Text GuiWinLoseText
    {
        get
        {
            return guiWinLoseText;
        }

        set
        {
            guiWinLoseText = value;
        }
    }
    public Text GuiEndData
    {
        get
        {
            return guiEndData;
        }

        set
        {
            guiEndData = value;
        }
    }
    public GameObject GuiMenuWinLose
    {
        get
        {
            return guiMenuWinLose;
        }

        set
        {
            guiMenuWinLose = value;
        }
    }
    public GameObject IngameGui
    {
        get
        {
            return ingameGui;
        }

        set
        {
            ingameGui = value;
        }
    }
    public GameObject GuiPrimerTutorial
    {
        get
        {
            return guiPrimerTutorial;
        }

        set
        {
            guiPrimerTutorial = value;
        }
    }
    public GameObject GuiSegundoTutorial
    {
        get
        {
            return guiSegundoTutorial;
        }

        set
        {
            guiSegundoTutorial = value;
        }
    }
    public GameObject Particulas
    {
        get
        {
            return particulas;
        }

        set
        {
            particulas = value;
        }
    }

    private Gradient GradienteTexto { set; get; }
    #endregion

    #region METODOS UNITY
    // Use this for initialization
    void Awake()
    {
        this.Partida = GameObject.FindGameObjectWithTag("partida").GetComponent<Partida>();
        this.PrimerTutorialAnimator = this.GuiPrimerTutorial.GetComponent<Animator>();
        this.SegundoTutorialAnimator = this.GuiSegundoTutorial.GetComponent<Animator>();
        this.IngameGuiAnimator = this.IngameGui.GetComponent<Animator>();
        this.CrearGradienteColores();
    }

    /// <summary>
    /// Crea la gradiente de colores que se va a utilizar para los modos de juego
    /// </summary>
    private void CrearGradienteColores()
    {
        GradientColorKey[] gck;
        GradientAlphaKey[] gak;
        this.GradienteTexto = new Gradient();
        gck = new GradientColorKey[6];
        gck[0].color = new Color32(162, 94, 223, 255);
        gck[0].time = 0.0F;
        gck[1].color = new Color32(88, 121, 244, 255);
        gck[1].time = 0.2F;
        gck[2].color = new Color32(86, 208, 132, 255);
        gck[2].time = 0.4F;
        gck[3].color = new Color32(231, 231, 88, 255);
        gck[3].time = 0.6F;
        gck[4].color = new Color32(253, 151, 73, 255);   
        gck[4].time = 0.8F;
        gck[5].color = new Color32(242,82,82, 255);
        gck[5].time = 1.0F;
        gak = new GradientAlphaKey[1];
        gak[0].alpha = 1.0F;
        gak[0].time = 1.0F;
        this.GradienteTexto.SetKeys(gck, gak);
    }

    private void Start()
    {
        this.MostrarIngameGui();
    }

    // Update is called once per frame
    void Update()
    {
        this.ActualizarReloj();
        this.ActualizarCantidadMovimientos();
    }
    #endregion

    #region METODOS
    /// <summary>
    /// Muestra la interfaz ingame
    /// </summary>
    private void MostrarIngameGui()
    {
        this.IngameGuiAnimator.SetBool("mostrar", true);
    }

    /// <summary>
    /// Oculta la itnerfaz ingame
    /// </summary>
    private void OcultarIngameGui()
    {
        this.IngameGuiAnimator.SetBool("mostrar", false);
    }

    /// <summary>
    /// Actualiza la cantidad de movimientos
    /// </summary>
    private void ActualizarCantidadMovimientos()
    {
        if (this.Partida.ModoJuegoSeleccionado.Equals(2))
        {
            var valorParaMostrar = this.Partida.CantidadMovimientosLimite - this.Partida.CantidadMovimientosCubo();
            this.GuiCantMovimiento.color = this.GradienteTexto.Evaluate((float)(1f - (valorParaMostrar / 10f)));

            this.GuiCantMovimiento.text = valorParaMostrar.ToString();
        }
        else
            this.GuiCantMovimiento.text = this.Partida.CantidadMovimientosCubo().ToString();
    }

    /// <summary>
    /// Actualiza el reloj de la partida con el formato 00:00
    /// </summary>
    /// <param name="tiempo"></param>
    private void ActualizarReloj()
    {
        if(this.Partida.ModoJuegoSeleccionado.Equals(1))
        {
            var valorParaMostrar = this.Partida.TiempoLimite - this.Partida.TiempoPartida;
            this.GuiTiempo.color = this.GradienteTexto.Evaluate((float)(1f - ((valorParaMostrar * 100) / this.Partida.TiempoLimite) / 100f));

            this.GuiTiempo.text = this.FormatearTiempoPartida(valorParaMostrar);
        }
        else
            this.GuiTiempo.text = this.FormatearTiempoPartida(this.Partida.TiempoPartida);
    }

    /// <summary>
    /// Calcula el tiempo en minutos:segundos
    /// </summary>
    /// <returns></returns>
    private string FormatearTiempoPartida(float tiempo)
    {
        float minutos = Mathf.Floor(tiempo / 60);
        float segundos = Mathf.Floor(tiempo % 60);
        float milisegundos = Mathf.Floor((tiempo*1000)%1000);

        if (minutos < 0)
            minutos = 0;

        if (segundos < 0)
            segundos = 0;

        if (milisegundos < 0)
            milisegundos = 0;

        return minutos.ToString("00") + ":" + segundos.ToString("00") + ":" + milisegundos.ToString("000");
    }

    /// <summary>
    /// Indica a la partida que el juego esta en pausa
    /// Abre el menu
    /// </summary>
    public void Pausa()
    {
        this.Partida.OcultarCuboMagico();
        this.OcultarIngameGui();
        this.Partida.Pausar();
        this.GuiMenuPausa.SetActive(true);        
    }

    /// <summary>
    /// Indica a la partida que debe reiniciarse cuando quiere terminar la actual y empezar otra
    /// </summary>
    public void Restart()
    {
        this.Partida.Restart();
    }

    /// <summary>
    /// Indica a la partida que debe reiniciarse cuando termina
    /// </summary>
    public void Resetear()
    {
        this.Partida.Resetear();
    }

    /// <summary>
    /// Indica a la partida que debe irse al menu
    /// </summary>
    public void Exit()
    {
        this.Partida.Exit();
    }

    /// <summary>
    /// Activa el menu de WinLose
    /// </summary>
    public void ActivarMenuWinLose(bool gano)
    {
        this.OcultarIngameGui();
        this.GuiWinLoseText.text = (gano) ? "You Win !" : "Game Over";
        this.GuiEndData.text = "Time " + this.FormatearTiempoPartida(this.Partida.TiempoPartida) + "\nMovements: " + (this.Partida.CantidadMovimientosCubo()).ToString();
        this.GuiMenuWinLose.SetActive(true);        
    }

    /// <summary>
    /// Activa el menu de WinLose con el mensaje final recibido
    /// </summary>
    public void ActivarMenuWinLose(string mensajeFinal)
    {
        this.OcultarIngameGui();
        this.GuiWinLoseText.text = mensajeFinal;
        this.GuiEndData.text = "Time " + this.FormatearTiempoPartida(this.Partida.TiempoPartida) + "\nMovements: " + (this.Partida.CantidadMovimientosCubo() + 1).ToString();
        this.GuiMenuWinLose.SetActive(true);     
    }

    public void MostrarTutorial()
    {
        this.GuiPrimerTutorial.SetActive(true);
        this.PrimerTutorialAnimator.Play("mostrar");
    }

    public void SiguienteTutorial()
    {
        this.Particulas.SetActive(true);
        this.GuiPrimerTutorial.SetActive(false);
        this.GuiSegundoTutorial.SetActive(true);
        this.SegundoTutorialAnimator.Play("mostrar");
    }

    public void TerminarTutorial()
    {
        this.GuiSegundoTutorial.SetActive(false);
        this.Particulas.SetActive(false);
    }
    #endregion
}
